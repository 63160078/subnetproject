/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.subnetcal;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Ow
 */
public class SubnetID {

    private int octets1;
    private int octets2;
    private int octets3;
    private int octets4;
    private int mask;
    private int mask128 = 10000000;
    private int mask192 = 11000000;
    private int mask224 = 11100000;
    private int mask240 = 11110000;
    private int mask248 = 11111000;
    private int mask252 = 11111100;
    private int mask254 = 11111110;
    private int mask255 = 11111111;

    public SubnetID(int Octets1, int Octets2, int Octets3, int Octets4, int mask) {
        this.octets1 = Octets1;
        this.octets2 = Octets2;
        this.octets3 = Octets3;
        this.octets4 = Octets4;
        this.mask = mask;
    }

    public int getMask() {
        return mask;
    }

    public void setMask(int mask) {
        this.mask = mask;
    }

    public int getOctets1() {
        return octets1;
    }

    public void setOctets1(int Octets1) {
        this.octets1 = Octets1;
    }

    public int getOctets2() {
        return octets2;
    }

    public void setOctets2(int Octets2) {
        this.octets2 = Octets2;
    }

    public int getOctets3() {
        return octets3;
    }

    public void setOctets3(int Octets3) {
        this.octets3 = Octets3;
    }

    public int getOctets4() {
        return octets4;
    }

    public void setOctets4(int Octets4) {
        this.octets4 = Octets4;
    }

    @Override
    public String toString() {
        return "IP address : " + octets1 + "." + octets2 + "." + octets3 + "." + octets4;
    }

    public String convertNetworkAddress(int octets4) {

        String subnet = "";
        String wild = "";
        String binarySub = "";

        switch (mask) {
            case 1:
                subnet = "Subnet mask : 128.0.0.0";
                wild = "  / Wild mask : 127.255.255.255";
                binarySub = "  / Binary Subnet Mask : 10000000 00000000 00000000 00000000";
                break;
            case 2:
                subnet = "Subnet mask : 192.0.0.0";
                wild = "  / Wild mask : 63.255.255.255";
                binarySub = "  / Binary Subnet Mask : 11000000 00000000 00000000 00000000";
                break;
            case 3:
                subnet = "Subnet mask : 224.0.0.0";
                wild = "  / Wild mask : 31.255.255.255";
                binarySub = "  / Binary Subnet Mask : 11100000 00000000 00000000 00000000";
                break;
            case 4:
                subnet = "Subnet mask : 240.0.0.0";
                wild = "  / Wild mask : 15.255.255.255";
                binarySub = "  / Binary Subnet Mask : 11110000 00000000 00000000 00000000";
                break;
            case 5:
                subnet = "Subnet mask : 248.0.0.0";
                wild = "  / Wild mask : 7.255.255.255";
                binarySub = "  / Binary Subnet Mask : 11111000 00000000 00000000 00000000";
                break;
            case 6:
                subnet = "Subnet mask : 252.0.0.0";
                wild = "  / Wild mask : 3.255.255.255";
                binarySub = "  / Binary Subnet Mask : 11111100 00000000 00000000 00000000";
                break;
            case 7:
                subnet = "Subnet mask : 254.0.0.0";
                wild = "  / Wild mask : 1.255.255.255";
                binarySub = "  / Binary Subnet Mask : 11111110 00000000 00000000 00000000";
                break;
            // A
            case 8:
                subnet = "Subnet mask : 255.0.0.0";
                wild = "  / Wild mask : 0.255.255.255";
                binarySub = "  / Binary Subnet Mask : 11111111 00000000 00000000 00000000";
                break;
            case 9:
                subnet = "Subnet mask : 255.128.0.0";
                wild = "  / Wild mask : 0.127.255.255";
                binarySub = "  / Binary Subnet Mask : 11111111 10000000 00000000 00000000";
                break;
            case 10:
                subnet = "Subnet mask : 255.192.0.0";
                wild = "  / Wild mask : 0.63.255.255";
                binarySub = "  / Binary Subnet Mask : 11111111 11000000 00000000 00000000";
                break;
            case 11:
                subnet = "Subnet mask : 255.224.0.0";
                wild = "  / Wild mask : 0.31.255.255";
                binarySub = "  / Binary Subnet Mask : 11111111 11100000 00000000 00000000";
                break;
            case 12:
                subnet = "Subnet mask : 255.240.0.0";
                wild = "  / Wild mask : 0.15.255.255";
                binarySub = "  / Binary Subnet Mask : 11111111 11110000 00000000 00000000";
                break;
            case 13:
                subnet = "Subnet mask : 255.248.0.0";
                wild = "  / Wild mask : 0.7.255.255";
                binarySub = "  / Binary Subnet Mask : 11111111 11111000 00000000 00000000";
                break;
            case 14:
                subnet = "Subnet mask : 255.252.0.0";
                wild = "  / Wild mask : 0.3.255.255";
                binarySub = "  / Binary Subnet Mask : 11111111 11111100 00000000 00000000";
                break;
            case 15:
                subnet = "Subnet mask : 255.254.0.0";
                wild = "  / Wild mask : 0.1.255.255";
                binarySub = "  / Binary Subnet Mask : 11111111 11111110 00000000 00000000";
                break;
            // B
            case 16:
                subnet = "Subnet mask : 255.255.0.0";
                wild = "  / Wild mask : 0.0.255.255";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 00000000 00000000";
                break;
            case 17:
                subnet = "Subnet mask : 255.255.128.0";
                wild = "  / Wild mask : 0.0.127.255";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 10000000 00000000";
                break;
            case 18:
                subnet = "Subnet mask : 255.255.192.0";
                wild = "  / Wild mask : 0.0.63.255";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 11000000 00000000";
                break;
            case 19:
                subnet = "Subnet mask : 255.255.224.0";
                wild = "  / Wild mask : 0.0.31.255";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 11100000 00000000";
                break;
            case 20:
                subnet = "Subnet mask : 255.255.240.0";
                wild = "  / Wild mask : 0.0.15.255";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 11110000 00000000";
                break;
            case 21:
                subnet = "Subnet mask : 255.255.248.0";
                wild = "  / Wild mask : 0.0.7.255";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 11111000 00000000";
                break;
            case 22:
                subnet = "Subnet mask : 255.255.252.0";
                wild = "  / Wild mask : 0.0.3.255";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 11111100 00000000";
                break;
            case 23:
                subnet = "Subnet mask : 255.255.254.0";
                wild = "  / Wild mask : 0.0.1.255";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 11111110 00000000";
                break;
            // C
            case 24:
                subnet = "Subnet mask : 255.255.255.0";
                wild = "  / Wild mask : 0.0.0.255";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 11111111  00000000";
                break;
            case 25:
                subnet = "Subnet mask : 255.255.255.128";
                wild = "  / Wild mask : 0.0.0.127";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 11111111  10000000";
                break;
            case 26:
                subnet = "Subnet mask : 255.255.255.192";
                wild = "  / Wild mask : 0.0.0.31";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 11111111  11000000";
                break;
            case 27:
                subnet = "Subnet mask : 255.255.255.224";
                wild = "  / Wild mask : 0.0.0.15";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 11111111  11100000";
                break;
            case 28:
                subnet = "Subnet mask : 255.255.255.240";
                wild = "  / Wild mask : 0.0.0.7";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 11111111  11110000";
                break;
            case 29:
                subnet = "Subnet mask : 255.255.255.248";
                wild = "  / Wild mask : 0.0.0.3";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 11111111  11111000";
                break;
            case 30:
                subnet = "Subnet mask : 255.255.255.252";
                wild = "  / Wild mask : 0.0.0.1";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 11111111  11111100";
                break;
            case 31:
                subnet = "Subnet mask : 255.255.255.254";
                wild = "  / Wild mask : 0.0.0.127";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 11111111  11111110";
                break;
            case 32:
                subnet = "Subnet mask : 255.255.255.255";
                wild = "  / Wild mask : 0.0.0.0";
                binarySub = "  / Binary Subnet Mask : 11111111 11111111 11111111  11111111";
                break;
        }
        return subnet + wild + binarySub;
    }

    public String printCIDR() {
        return "CIDR Notation : /" + mask;
    }

    public String printType(int firstOctets, int secondOctets) {
        if (firstOctets == 10) {
            return "IP Type : Private";
        } else if (firstOctets == 172) {
            if (secondOctets == 16) {
                return "IP Type : Private";
            } else if (secondOctets == 17) {
                return "IP Type : Private";
            } else if (secondOctets == 18) {
                return "IP Type : Private";
            } else if (secondOctets == 19) {
                return "IP Type : Private";
            } else if (secondOctets == 20) {
                return "IP Type : Private";
            } else if (secondOctets == 21) {
                return "IP Type : Private";
            } else if (secondOctets == 22) {
                return "IP Type : Private";
            } else if (secondOctets == 23) {
                return "IP Type : Private";
            } else if (secondOctets == 24) {
                return "IP Type : Private";
            } else if (secondOctets == 25) {
                return "IP Type : Private";
            } else if (secondOctets == 26) {
                return "IP Type : Private";
            } else if (secondOctets == 27) {
                return "IP Type : Private";
            } else if (secondOctets == 28) {
                return "IP Type : Private";
            } else if (secondOctets == 29) {
                return "IP Type : Private";
            } else if (secondOctets == 30) {
                return "IP Type : Private";
            } else if (secondOctets == 31) {
                return "IP Type : Private";
            } else {
                return "IP Type : Public";
            }
        } else if (firstOctets == 192 && secondOctets == 168) {
            return "IP Type : Private";
        } else {
            return "IP Type : Public";
        }
    }

    public String printShortIP() {
        return "IP address : " + octets1 + "." + octets2 + "." + octets3 + "." + octets4 + " /" + mask;
    }

    public String printBinaryID() {
        return "Binary ID : " + toBinary(octets1) + toBinary(octets2) + toBinary(octets3) + toBinary(octets4);
    }

    public static String toBinary(int x) {
        return StringUtils.leftPad(Integer.toBinaryString(x), 8, '0');
    }

    public String printIntegerID() {
        String binaryStr = toBinary(octets1) + toBinary(octets2) + toBinary(octets3) + toBinary(octets4);
        long intID = Long.parseLong(binaryStr, 2);
        return "Integer ID : " + intID;
    }

    public String printHexID() {
        String binaryStr = toBinary(octets1) + toBinary(octets2) + toBinary(octets3) + toBinary(octets4);
        long intID = Long.parseLong(binaryStr, 2);
        String hexID = Integer.toHexString((int) intID);
        return "Hex ID : 0x" + hexID;
    }

    public String printClass(int firstOctets) {
        if (firstOctets <= 127) {
            return "IP class : A";
        } else if (firstOctets >= 128 && firstOctets <= 191) {
            return "IP class : B";
        } else if (firstOctets >= 192 && firstOctets <= 223) {
            return "IP class : C";
        }
        return "IP class : Not Avaliable";
    }

    public String printNetworkAddress(int Octets1, int Octets2, int Octets3, int Octets4, int mask) {
        int mask1 = 0;
        int mask2 = 0;
        int mask3 = 0;
        int mask4 = 0;

        octets1 = Integer.parseInt(toBinary(Octets1));
        octets2 = Integer.parseInt(toBinary(Octets2));
        octets3 = Integer.parseInt(toBinary(Octets3));
        octets4 = Integer.parseInt(toBinary(Octets4));
        switch (mask) {
            case 1:
                mask1 = 128;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 2:
                mask1 = 192;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 3:
                mask1 = 224;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 4:
                mask1 = 240;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 5:
                mask1 = 248;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 6:
                mask1 = 252;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 7:
                mask1 = 254;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
// A
            case 8:
                mask1 = 255;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 9:
                mask1 = 255;
                mask2 = 128;
                mask3 = 0;
                mask4 = 0;
                break;
            case 10:
                mask1 = 255;
                mask2 = 192;
                mask3 = 0;
                mask4 = 0;
                break;
            case 11:
                mask1 = 255;
                mask2 = 224;
                mask3 = 0;
                mask4 = 0;
                break;
            case 12:
                mask1 = 255;
                mask2 = 240;
                mask3 = 0;
                mask4 = 0;
                break;
            case 13:
                mask1 = 255;
                mask2 = 248;
                mask3 = 0;
                mask4 = 0;
                break;
            case 14:
                mask1 = 255;
                mask2 = 252;
                mask3 = 0;
                mask4 = 0;
                break;
            case 15:
                mask1 = 255;
                mask2 = 254;
                mask3 = 0;
                mask4 = 0;
                break;
// B
            case 16:
                mask1 = 255;
                mask2 = 255;
                mask3 = 0;
                mask4 = 0;
                break;
            case 17:
                mask1 = 255;
                mask2 = 255;
                mask3 = 128;
                mask4 = 0;
                break;
            case 18:
                mask1 = 255;
                mask2 = 255;
                mask3 = 192;
                mask4 = 0;
                break;
            case 19:
                mask1 = 255;
                mask2 = 255;
                mask3 = 224;
                mask4 = 0;
                break;
            case 20:
                mask1 = 255;
                mask2 = 255;
                mask3 = 240;
                mask4 = 0;
                break;
            case 21:
                mask1 = 255;
                mask2 = 255;
                mask3 = 248;
                mask4 = 0;
                break;
            case 22:
                mask1 = 255;
                mask2 = 255;
                mask3 = 252;
                mask4 = 0;
                break;
            case 23:
                mask1 = 255;
                mask2 = 255;
                mask3 = 254;
                mask4 = 0;
                break;
// C
            case 24:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 0;
                break;
            case 25:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 128;
                break;
            case 26:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 192;
                break;
            case 27:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 224;
                break;
            case 28:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 240;
                break;
            case 29:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 248;
                break;
            case 30:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 252;
                break;
            case 31:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 254;
                break;
            case 32:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
        }
        octets1 = Octets1 & mask1;
        octets2 = Octets2 & mask2;
        octets3 = Octets3 & mask3;
        octets4 = Octets4 & mask4;
        return "Network Address : " + octets1 + "." + octets2 + "." + octets3 + "." + octets4;
    }

    public String printNetworkAddressRange(int Octets1, int Octets2, int Octets3, int Octets4, int mask) {
        int mask1 = 0;
        int mask2 = 0;
        int mask3 = 0;
        int mask4 = 0;

        octets1 = Integer.parseInt(toBinary(Octets1));
        octets2 = Integer.parseInt(toBinary(Octets2));
        octets3 = Integer.parseInt(toBinary(Octets3));
        octets4 = Integer.parseInt(toBinary(Octets4));

        switch (mask) {
            case 1:
                mask1 = 128;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 2:
                mask1 = 192;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 3:
                mask1 = 224;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 4:
                mask1 = 240;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 5:
                mask1 = 248;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 6:
                mask1 = 252;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 7:
                mask1 = 254;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
// A
            case 8:
                mask1 = 255;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
            case 9:
                mask1 = 255;
                mask2 = 128;
                mask3 = 0;
                mask4 = 0;
                break;
            case 10:
                mask1 = 255;
                mask2 = 192;
                mask3 = 0;
                mask4 = 0;
                break;
            case 11:
                mask1 = 255;
                mask2 = 224;
                mask3 = 0;
                mask4 = 0;
                break;
            case 12:
                mask1 = 255;
                mask2 = 240;
                mask3 = 0;
                mask4 = 0;
                break;
            case 13:
                mask1 = 255;
                mask2 = 248;
                mask3 = 0;
                mask4 = 0;
                break;
            case 14:
                mask1 = 255;
                mask2 = 252;
                mask3 = 0;
                mask4 = 0;
                break;
            case 15:
                mask1 = 255;
                mask2 = 254;
                mask3 = 0;
                mask4 = 0;
                break;
// B
            case 16:
                mask1 = 255;
                mask2 = 255;
                mask3 = 0;
                mask4 = 0;
                break;
            case 17:
                mask1 = 255;
                mask2 = 255;
                mask3 = 128;
                mask4 = 0;
                break;
            case 18:
                mask1 = 255;
                mask2 = 255;
                mask3 = 192;
                mask4 = 0;
                break;
            case 19:
                mask1 = 255;
                mask2 = 255;
                mask3 = 224;
                mask4 = 0;
                break;
            case 20:
                mask1 = 255;
                mask2 = 255;
                mask3 = 240;
                mask4 = 0;
                break;
            case 21:
                mask1 = 255;
                mask2 = 255;
                mask3 = 248;
                mask4 = 0;
                break;
            case 22:
                mask1 = 255;
                mask2 = 255;
                mask3 = 252;
                mask4 = 0;
                break;
            case 23:
                mask1 = 255;
                mask2 = 255;
                mask3 = 254;
                mask4 = 0;
                break;
// C
            case 24:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 0;
                break;
            case 25:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 128;
                break;
            case 26:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 192;
                break;
            case 27:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 224;
                break;
            case 28:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 240;
                break;
            case 29:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 248;
                break;
            case 30:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 252;
                break;
            case 31:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 254;
                break;
            case 32:
                mask1 = 255;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
        }

        octets1 = Octets1 & mask1;
        octets2 = Octets2 & mask2;
        octets3 = Octets3 & mask3;
        octets4 = Octets4 & mask4;
        int range = octets4 + 1;
        return octets1 + "." + octets2 + "." + octets3 + "." + range;

    }

    public String printBroadcastAddress(int Octets1, int Octets2, int Octets3, int Octets4, int mask) {
        int mask1 = 0;
        int mask2 = 0;
        int mask3 = 0;
        int mask4 = 0;

        octets1 = Integer.parseInt(toBinary(Octets1));
        octets2 = Integer.parseInt(toBinary(Octets2));
        octets3 = Integer.parseInt(toBinary(Octets3));
        octets4 = Integer.parseInt(toBinary(Octets4));

        switch (mask) {
            case 1:
                mask1 = 127;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 2:
                mask1 = 63;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 3:
                mask1 = 31;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 4:
                mask1 = 15;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 5:
                mask1 = 7;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 6:
                mask1 = 3;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 7:
                mask1 = 1;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            //class A
            case 8:
                mask1 = 0;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 9:
                mask1 = 0;
                mask2 = 127;
                mask3 = 255;
                mask4 = 255;
                break;
            case 10:
                mask1 = 0;
                mask2 = 63;
                mask3 = 255;
                mask4 = 255;
                break;
            case 11:
                mask1 = 0;
                mask2 = 31;
                mask3 = 255;
                mask4 = 255;
                break;
            case 12:
                mask1 = 0;
                mask2 = 15;
                mask3 = 255;
                mask4 = 255;
                break;
            case 13:
                mask1 = 0;
                mask2 = 7;
                mask3 = 255;
                mask4 = 255;
                break;
            case 14:
                mask1 = 0;
                mask2 = 3;
                mask3 = 255;
                mask4 = 255;
                break;
            case 15:
                mask1 = 0;
                mask2 = 1;
                mask3 = 255;
                mask4 = 255;
                break;
//class B
            case 16:
                mask1 = 0;
                mask2 = 0;
                mask3 = 255;
                mask4 = 255;
                break;
            case 17:
                mask1 = 0;
                mask2 = 0;
                mask3 = 127;
                mask4 = 255;
                break;
            case 18:
                mask1 = 0;
                mask2 = 0;
                mask3 = 63;
                mask4 = 255;
                break;
            case 19:
                mask1 = 0;
                mask2 = 0;
                mask3 = 31;
                mask4 = 255;
                break;
            case 20:
                mask1 = 0;
                mask2 = 0;
                mask3 = 15;
                mask4 = 255;
                break;
            case 21:
                mask1 = 0;
                mask2 = 0;
                mask3 = 7;
                mask4 = 255;
                break;
            case 22:
                mask1 = 0;
                mask2 = 0;
                mask3 = 3;
                mask4 = 255;
                break;
            case 23:
                mask1 = 0;
                mask2 = 0;
                mask3 = 1;
                mask4 = 255;
                break;
//class C
            case 24:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 255;
                break;
            case 25:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 127;
                break;
            case 26:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 63;
                break;
            case 27:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 31;
                break;
            case 28:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 15;
                break;
            case 29:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 7;
                break;
            case 30:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 3;
                break;
            case 31:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 1;
                break;
            case 32:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
        }

        octets1 = Octets1 | mask1;
        octets2 = Octets2 | mask2;
        octets3 = Octets3 | mask3;
        octets4 = Octets4 | mask4;

        return "Broadcast Address : " + octets1 + "." + octets2 + "." + octets3 + "." + octets4;
    }

    public String printBroadcastAddressRange(int Octets1, int Octets2, int Octets3, int Octets4, int mask) {
        int mask1 = 0;
        int mask2 = 0;
        int mask3 = 0;
        int mask4 = 0;

        octets1 = Integer.parseInt(toBinary(Octets1));
        octets2 = Integer.parseInt(toBinary(Octets2));
        octets3 = Integer.parseInt(toBinary(Octets3));
        octets4 = Integer.parseInt(toBinary(Octets4));

        switch (mask) {
            case 1:
                mask1 = 127;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 2:
                mask1 = 63;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 3:
                mask1 = 31;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 4:
                mask1 = 15;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 5:
                mask1 = 7;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 6:
                mask1 = 3;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 7:
                mask1 = 1;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
// A
            case 8:
                mask1 = 0;
                mask2 = 255;
                mask3 = 255;
                mask4 = 255;
                break;
            case 9:
                mask1 = 0;
                mask2 = 127;
                mask3 = 255;
                mask4 = 255;
                break;
            case 10:
                mask1 = 0;
                mask2 = 63;
                mask3 = 255;
                mask4 = 255;
                break;
            case 11:
                mask1 = 0;
                mask2 = 31;
                mask3 = 255;
                mask4 = 255;
                break;
            case 12:
                mask1 = 0;
                mask2 = 15;
                mask3 = 255;
                mask4 = 255;
                break;
            case 13:
                mask1 = 0;
                mask2 = 7;
                mask3 = 255;
                mask4 = 255;
                break;
            case 14:
                mask1 = 0;
                mask2 = 3;
                mask3 = 255;
                mask4 = 255;
                break;
            case 15:
                mask1 = 0;
                mask2 = 1;
                mask3 = 255;
                mask4 = 255;
                break;
// B
            case 16:
                mask1 = 0;
                mask2 = 0;
                mask3 = 255;
                mask4 = 255;
                break;
            case 17:
                mask1 = 0;
                mask2 = 0;
                mask3 = 127;
                mask4 = 255;
                break;
            case 18:
                mask1 = 0;
                mask2 = 0;
                mask3 = 63;
                mask4 = 255;
                break;
            case 19:
                mask1 = 0;
                mask2 = 0;
                mask3 = 31;
                mask4 = 255;
                break;
            case 20:
                mask1 = 0;
                mask2 = 0;
                mask3 = 15;
                mask4 = 255;
                break;
            case 21:
                mask1 = 0;
                mask2 = 0;
                mask3 = 7;
                mask4 = 255;
                break;
            case 22:
                mask1 = 0;
                mask2 = 0;
                mask3 = 3;
                mask4 = 255;
                break;
            case 23:
                mask1 = 0;
                mask2 = 0;
                mask3 = 1;
                mask4 = 255;
                break;
// C
            case 24:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 255;
                break;
            case 25:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 127;
                break;
            case 26:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 63;
                break;
            case 27:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 31;
                break;
            case 28:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 15;
                break;
            case 29:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 7;
                break;
            case 30:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 3;
                break;
            case 31:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 1;
                break;
            case 32:
                mask1 = 0;
                mask2 = 0;
                mask3 = 0;
                mask4 = 0;
                break;
        }

        octets1 = Octets1 | mask1;
        octets2 = Octets2 | mask2;
        octets3 = Octets3 | mask3;
        octets4 = Octets4 | mask4;
        int range = octets4 - 1;
        return octets1 + "." + octets2 + "." + octets3 + "." + range;
    }

    public String printTotalHostUsable(int mask) {
        
        String host = "";
        String usable = "";
        
        switch (mask) {
            case 1:
                host = "Total num (hosts) : 2,147,483,648 ";
                usable = " Num of usable (hosts) : 2,147,483,646";
                break;
            case 2:
                host = "Total num (hosts) : 1,073,741,824 ";
                usable = " Num of usable (hosts) : 1,073,741,822";
                break;
            case 3:
                host = "Total num (hosts) : 536,870,912 ";
                usable = " Num of usable (hosts) : 536,870,910";
                break;
            case 4:
                host = "Total num (hosts) : 268,435,456 ";
                usable = " Num of usable (hosts) : 268,435,454";
                break;
            case 5:
                host = "Total num (hosts) : 134,217,728 ";
                usable = " Num of usable (hosts) : 134,217,726";
                break;
            case 6:
                host = "Total num (hosts) : 67,108,864 ";
                usable = " Num of usable (hosts) : 67,108,862";
                break;
            case 7:
                host = "Total num (hosts) : 33,554,432 ";
                usable = " Num of usable (hosts) : 33,554,430";
                break;
// A
            case 8:
                host = "Total num (hosts) : 16,777,216 ";
                usable = " Num of usable (hosts) : 16,777,214";
                break;
            case 9:
                host = "Total num (hosts) : 8,388,608 ";
                usable = " Num of usable (hosts) : 8,388,606";
                break;
            case 10:
                host = "Total num (hosts) : 4,194,304 ";
                usable = " Num of usable (hosts) : 4,194,302";
                break;
            case 11:
                host = "Total num (hosts) : 2,097,152 ";
                usable = " Num of usable (hosts) : 2,097,150";
                break;
            case 12:
                host = "Total num (hosts) : 1,048,576 ";
                usable = " Num of usable (hosts) : 1,048,574";
                break;
            case 13:
                host = "Total num (hosts) : 524,288 ";
                usable = " Num of usable (hosts) : 524,286";
                break;
            case 14:
                host = "Total num (hosts) : 262,144 ";
                usable = " Num of usable (hosts) : 262,142";
                break;
            case 15:
                host = "Total num (hosts) : 131,072 ";
                usable = " Num of usable (hosts) : 131,070";
                break;
// B
            case 16:
                host = "Total num (hosts) : 65,536 ";
                usable = " Num of usable (hosts) : 65,534";
                break;
            case 17:
                host = "Total num (hosts) : 32,768 ";
                usable = " Num of usable (hosts) : 32,766";
                break;
            case 18:
                host = "Total num (hosts) : 16,384 ";
                usable = " Num of usable (hosts) : 16,382";
                break;
            case 19:
                host = "Total num (hosts) : 8,192 ";
                usable = " Num of usable (hosts) : 8,190";
                break;
            case 20:
                host = "Total num (hosts) : 4,096 ";
                usable = " Num of usable (hosts) : 4,094";
                break;
            case 21:
                host = "Total num (hosts) : 2,048 ";
                usable = " Num of usable (hosts) : 2,046";
                break;
            case 22:
                host = "Total num (hosts) : 1,024 ";
                usable = " Num of usable (hosts) : 1,022";
                break;
            case 23:
                host = "Total num (hosts) : 512 ";
                usable = " Num of usable (hosts) : 510";
                break;
// C
            case 24:
                host = "Total num (hosts) : 256 ";
                usable = " Num of usable (hosts) : 254";
                break;
            case 25:
                host = "Total num (hosts) : 128 ";
                usable = " Num of usable (hosts) : 126";
                break;
            case 26:
                host = "Total num (hosts) : 64 ";
                usable = " Num of usable (hosts) : 62";
                break;
            case 27:
                host = "Total num (hosts) : 32 ";
                usable = "  Num of usable (hosts) : 30";
                break;
            case 28:
                host = "Total num (hosts) : 16 ";
                usable = " Num of usable (hosts) : 14";
                break;
            case 29:
                host = "Total num (hosts) : 8 ";
                usable = " Num of usable (hosts) : 6";
                break;
            case 30:
                host = "Total num (hosts) : 4 ";
                usable = " Num of usable (hosts) : 2";
                break;
            case 31:
                host = "Total num (hosts) : 2 ";
                usable = " Num of usable (hosts) : 0";
                break;
            case 32:
                host = "Total num (hosts) : 1 ";
                usable = " Num of usable (hosts) : 0";
                break;
        }
        return host + usable;
    }

    public String printIPRange(int octets1, int octets2, int octets3, int octets4, int mask) {
        if (mask == 31 || mask == 32) {
            return "Usable host IP range : Not Avaliable";
        } else {
            return "Usable host IP range : " + printNetworkAddressRange(octets1, octets2, octets3, octets4, mask) + " - " + printBroadcastAddressRange(octets1, octets2, octets3, octets4, mask);
        }
    }

}
