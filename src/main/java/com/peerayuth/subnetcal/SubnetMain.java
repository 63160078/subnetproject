/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.subnetcal;

import java.util.Scanner;

/**
 *
 * @author Ow
 */
public class SubnetMain {

    public static void main(String[] args) {

        Scanner wn = new Scanner(System.in);
        System.out.println("Please input IP Address ( xxx . xxx . xxx . xxx  / xxx )");

        //scanner input
        int octets1 = wn.nextInt();
        int octets2 = wn.nextInt();
        int octets3 = wn.nextInt();
        int octets4 = wn.nextInt();
        int mask = wn.nextInt();

        //object id
        SubnetID id = new SubnetID(octets1, octets2, octets3, octets4, mask);

        //print output
        System.out.println(id);
        System.out.println(id.printNetworkAddress(octets1, octets2, octets3, octets4, mask));
        System.out.println(id.printIPRange(octets1, octets2, octets3, octets4, mask));
        System.out.println(id.printBroadcastAddress(octets1, octets2, octets3, octets4, mask));
        System.out.println(id.printTotalHostUsable(mask));
        System.out.println(id.convertNetworkAddress(octets4));
        
        System.out.println(id.printClass(octets1));
        System.out.println(id.printCIDR());
        System.out.println(id.printType(octets1, octets2));
        
        
        System.out.println("\n"+id.printShortIP());
        System.out.println(id.printBinaryID());
        System.out.println(id.printIntegerID());
        System.out.println(id.printHexID());
    }

}
